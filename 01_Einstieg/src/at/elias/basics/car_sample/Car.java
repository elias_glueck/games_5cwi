package at.elias.basics.car_sample;

public class Car {
	private String color;
	private int maxSpeed;
	private double basicprice;
	private Engine engine;
	private double usage;
	private double distance;
	private Manufacturer manufacturer;

	public Car(String color, int maxSpeed, double basicprice, Engine engine, double usage, double distance,
			Manufacturer manufacturer) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basicprice = basicprice;
		this.engine = engine;
		this.usage = usage;
		this.distance = distance;
		this.manufacturer = manufacturer;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public double getbasicprice() {
		return basicprice;
	}

	public void setbasicPrice(double basicprice) {
		this.basicprice = basicprice;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public double getUsage() {
		if (distance <= 50000) {
			return usage;
		} else {
			usage = usage * 1.098;
			return usage;
		}
	}

	public void setUsage(int usage) {
		this.usage = usage;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getBasicprice() {
		return basicprice;
	}

	public void setBasicprice(double basicprice) {
		this.basicprice = basicprice;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setUsage(double usage) {
		this.usage = usage;
	}

	public double getPrice() {
		double price;
		price = basicprice *(1 - manufacturer.getDiscount());
		return price;
	}
}