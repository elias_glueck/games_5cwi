package at.elias.basics.car_sample;

public class Engine {

	private String spirit;
	private int ps;
	
	
	public Engine(String spirit, int ps) {
		super();
		this.spirit = spirit;
		this.ps = ps;
	}

	public String getSpirit() {
		return spirit;
	}

	public void setSpirit(String spirit) {
		this.spirit = spirit;
	}

	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		this.ps = ps;
	}
	
	
	
}
