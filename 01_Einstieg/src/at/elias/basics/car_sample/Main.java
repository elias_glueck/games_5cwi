package at.elias.basics.car_sample;

import java.time.LocalDate;

public class Main {
	
	
	public static void main(String[] args) {
	Engine engine1 = new Engine ("Benzin",8);
	Engine engine2 = new Engine ("Diesel",10);
	
	Manufacturer producer1 = new Manufacturer("Audi","AT",5);
	Manufacturer producer2 = new Manufacturer("BMW","DE",10);
			
	
			Car car1 = new Car("blue", 300, 10000, engine1,100, 10000, producer1);
			Car car2 = new Car ("red", 250, 12000, engine2,100, 50000, producer2);
		 
			Car car3 = new Car ("red", 250, 12000, engine2,100, 50001, producer2);
	
			
		Person	p1 = new Person("Elias", "Glück", LocalDate.of(1999, 06, 29));
		Person	p2 = new Person("Matthias", "Böhler", LocalDate.of(2001, 06, 22));
		
			p1.addCar(car1);
			p1.addCar(car2);
			p2.addCar(car3);
			
			
//	System.out.println(car1.getEngine().getPs());
//	System.out.println(car1.getEngine().getSpirit());
//	System.out.println(car1.getUsage());
//	System.out.println(car1.getProducer().getName());
//	System.out.println(car1.getEngine().getSpirit());
//	System.out.println(car1.getPrice());
//	System.out.println(car1.getbasicprice());
//	System.out.println(car1.getUsage());
//	System.out.println(car2.getUsage());
//	System.out.println(car3.getUsage());
//	System.out.println(p1.getAge());
	p1.printCars();
	System.out.println(p1.getValueOfCars());
	
	
			}

}
