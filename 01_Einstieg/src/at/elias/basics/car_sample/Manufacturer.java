package at.elias.basics.car_sample;

public class Manufacturer {

	private String name;
	private String land;
	private double discount;
	
	public Manufacturer(String name, String land, double discount) {
		super();
		this.name = name;
		this.land = land;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public double getDiscount() {
		discount = discount * 0.01;
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
}
