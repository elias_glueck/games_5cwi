package at.elias.basics.car_sample;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {

	private String fristname, lastname;
	private LocalDate birthday;
	private List<Car> cars;


	public Person(String fristname, String lastname, LocalDate birthday) {
		super();
		this.fristname = fristname;
		this.lastname = lastname;
		this.birthday = birthday;
		cars = new ArrayList<Car>();
		
	}
	
	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getColor());
			}
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFristname() {
		return fristname;
	}

	public void setFristname(String fristname) {
		this.fristname = fristname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public int getAge() {
		Period period = Period.between(birthday, LocalDate.now());
		return period.getYears();
	}
	
	public int getValueOfCars() {
		int value = 0;
		for (Car car : cars) {
			value += car.getPrice();
		}
		return value;
	}
	

}

