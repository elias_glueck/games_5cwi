package at.elias.basics.test;

public class Engine {
	private int hp;
	private String type;
	
	public Engine(int hp, String type) {
		super();
		this.hp = hp;
		this.type = type;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
